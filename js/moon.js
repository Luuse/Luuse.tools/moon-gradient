Date.prototype.addDays = function(days) {
	var dat = new Date(this.valueOf())
	dat.setDate(dat.getDate() + days)
	return dat
}

function getDates(startDate, stopDate) {
	var dateArray = new Array()
	var currentDate = startDate
	while (currentDate <= stopDate) {
		dateArray.push(currentDate)
		currentDate = currentDate.addDays(1)
	}
	return dateArray
}
function getDateInterval(startDate, endDate) {
	var dateArray = getDates(new Date(startDate), new Date(endDate))
	var UL = document.getElementsByTagName('UL')[0]
	UL.innerHTML = ""
	var old = ''
	for (i = 0; i < dateArray.length; i ++ ) {
		var m = moon_phase(dateArray[i])
		if (m !== 'none') {
			UL.innerHTML += '<li class="'+m+'"></li>'
		}
		old = m
	}
}

function moon_phase(date) {
    var year = date.getYear(),
        month = date.getMonth(),
        day = date.getDay();

    if (month < 3) {
        year--;
        month += 12;
    }

    ++month;

    jd = 365.25 * year + 30.6 * month + day - 694039.09; // jd is total days elapsed
    jd /= 29.53; // divide by the moon cycle (29.53 days)
    phase = parseInt(jd, 10); // int(jd) -> phase, take integer part of jd
    jd -= phase; // subtract integer part to leave fractional part of original jd
    phase = Math.ceil(jd * 8); // scale fraction from 0-8 and round by adding 0.5
    phase = phase & 7; // 0 and 8 are the same so turn 8 into 0

    switch (phase) {
        case 0: phase = "New_Moon"; break;
        case 1: phase = "Waxing_Crescent_Moon"; break;
        case 2: phase = "Quarter_Moon"; break;
        case 3: phase = "Waxing_Gibbous_Moon"; break;
        case 4: phase = "Full_Moon"; break;
        case 5: phase = "Waning_Gibbous_Moon"; break;
        case 6: phase = "Last_Quarter_Moon";
        case 7: phase = "Waning_Crescent_Moon"; break;
    }
    return phase;
}

function allMoons(){
	let startDate = document.getElementById('start').value
	let endDate = document.getElementById('end').value
	getDateInterval(startDate, endDate)
}

getDateInterval('01-01-2000', '01-01-2005')

